﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace JsonToCsv.Test
{
    [TestClass]
    public class ConvertTest
    {
        [TestMethod]
        public void Convert_Should_Set_Headers_Correctly_For_Objects_With_The_Same_Proprties()
        {
            //Arrange

            var json = GetJsonSameProps();

            //Act

            var csv = new JsonToCsv().Convert(json);

            //Assert

            //TODO WD

            Console.WriteLine(csv);

        }

        [TestMethod]
        public void Convert_Should_Set_Headers_Correctly_For_Similar_Objects_With_Different_Properties()
        {
            //Arrange

            var json = GetJsonDiffProps();

            //Act

            var csv = new JsonToCsv().Convert(json);

            //Assert

            //TODO WD

            Console.WriteLine(csv);

        }

        [TestMethod]
        public void Convert_Should_Set_Headers_Correctly_For_Different_Objects()
        {
            //Arrange

            var json = GetJsonDiffObjs();

            //Act

            var csv = new JsonToCsv().Convert(json);

            //Assert

            //TODO WD

            Console.WriteLine(csv);

        }

        [TestMethod]
        public void Convert_Should_Not_Include_Nested_Arrays_When_NestedArrayHandling_Is_Ignore()
        {
            //Arrange

            var json = GetJsonNestedArrays();

            var settings = JsonToCsv.Settings.IncludeNested();
            settings.NestedArrayHandling = JsonToCsv.NestedArrayHandling.Ignore;

            //Act

            var csv = new JsonToCsv(settings).Convert(json);

            //Assert

            //TODO WD

            Console.WriteLine(csv);
        }

        [TestMethod]
        public void Convert_Should_Not_Include_Nested_Dictionaries_NestedDictionaryHandling_Is_Ignore()
        {
            //Arrange

            var json = GetJsonNestedDictionaries();

            var settings = JsonToCsv.Settings.IncludeNested();
            settings.NestedDictionaryHandling = JsonToCsv.NestedDictionaryHandling.Ignore;

            //Act

            var csv = new JsonToCsv(settings).Convert(json);

            //Assert

            //TODO WD

            Console.WriteLine(csv);
        }

        [TestMethod]
        public void Convert_Should_Not_Include_Headers_For_Excluded_Properties()
        {
            //Arrange

            var json = GetJsonSameProps();
            var settings = JsonToCsv.Settings.IgnoreNested();
            settings.ExcludeProperties("foo", "qux");

            //Act

            var csv = new JsonToCsv(settings).Convert(json);

            //Assert

            //TODO WD

            Console.WriteLine(csv);
        }

        [TestMethod]
        public void Convert_Should_Apply_Date_Type_Formatter()
        {
            //Arrange

            var json = GetJsonDataTypes();
            var settings = JsonToCsv.Settings.IncludeNested();
            settings.TypeFormatters.DateFormatter = dt => dt.ToShortDateString();

            //Act

            var csv = new JsonToCsv(settings).Convert(json);

            //Assert

            //TODO WD

            Console.WriteLine(csv);

        }

        [TestMethod]
        public void Convert_Should_Apply_Column_Formatters()
        {
            //Arrange

            var json = GetJsonDataTypes();
            var settings = JsonToCsv.Settings.IncludeNested();
            settings.ColumnFormatters.Add(s => Convert.ToDateTime(s).ToShortDateString(), "date", "dateTime");

            //Act

            var csv = new JsonToCsv(settings).Convert(json);

            //Assert

            //TODO WD

            Console.WriteLine(csv);
        }

        [TestMethod]
        [Description("Test the Column Formatters take priority over Type Formatters")]
        public void Convert_Should_Apply_Column_Formatters_Over_Type_Formatters()
        {
            //Arrange

            var json = GetJsonDataTypes();
            var settings = JsonToCsv.Settings.IncludeNested();
            settings.TypeFormatters.DateFormatter = dt => dt.ToShortDateString();
            settings.ColumnFormatters.Add(s => "Quack", "date", "dateTime");

            //Act

            var csv = new JsonToCsv(settings).Convert(json);

            //Assert

            //TODO WD

            Console.WriteLine(csv);
        }

        //TODO: Ordering tests?

        #region Helpers

        /// <summary>
        /// A json string where all items have the same properties (columns)
        /// </summary>
        /// <returns></returns>
        private string GetJsonSameProps()
        {
            return GetJsonFromFile("SameProps.json");
        }

        /// <summary>
        /// A json string where some items have different properties (columns). E.G. The first item may have props A,B, while the second item may have props A,B,C,D
        /// </summary>
        /// <returns></returns>
        private string GetJsonDiffProps()
        {
            return GetJsonFromFile("DiffProps.json");
        }

        /// <summary>
        /// A json string where the items in the array are different. E.G. The first item may have prop A,B while the second item may have props X,Y,Z
        /// </summary>
        /// <returns></returns>
        private string GetJsonDiffObjs()
        {
            return GetJsonFromFile("DiffObjs.json");
        }

        /// <summary>
        /// A json string containing nested arrays
        /// </summary>
        /// <returns></returns>
        private string GetJsonNestedArrays()
        {
            return GetJsonFromFile("NestedArrays.json");
        }

        /// <summary>
        /// A json string containing nested arrays
        /// </summary>
        /// <returns></returns>
        private string GetJsonNestedDictionaries()
        {
            return GetJsonFromFile("NestedDictionaries.json");
        }

        /// <summary>
        /// A json string containing nested arrays
        /// </summary>
        /// <returns></returns>
        private string GetJsonDataTypes()
        {
            return GetJsonFromFile("DataTypes.json");
        }

        private string GetJsonFromFile(string fileName)
        {
            return File.ReadAllText("Json/" + fileName);
        }

        #endregion

    }
}
