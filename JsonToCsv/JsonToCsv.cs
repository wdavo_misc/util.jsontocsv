﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json.Linq;

namespace JsonToCsv
{
    public class JsonToCsv
    {
        private readonly Settings _settings;

        /// <summary>
        /// Default Setings
        /// Nested are Ignored
        /// </summary>
        public JsonToCsv()
            : this(Settings.IgnoreNested())
        {

        }

        public JsonToCsv(Settings settings)
        {
            _settings = settings;
        }

        public string Convert(string jsonString)
        {
            if (string.IsNullOrWhiteSpace(jsonString))
                throw new ArgumentNullException("jsonString");

            JArray array;

            try
            {
                array = JArray.Parse(jsonString);
            }
            catch (Exception ex)
            {
                throw new ArgumentException("jsonString is not a valid JArray", "jsonString", ex);
            }

            return Convert(array);
        }

        public string Convert(JArray jsonArray)
        {
            var converter = new Converter(
                jsonArray,
                _settings.NestedArrayHandling,
                _settings.NestedDictionaryHandling,
                _settings.Excluded.Distinct().ToList(),
                _settings.TypeFormatters.ToDictionary(),
                _settings.ColumnFormatters.Formatters);

            return converter.Convert();
        }

        private class Converter
        {
            private readonly JArray _array;
            private readonly NestedArrayHandling _nestedArrayHandling;
            private readonly NestedDictionaryHandling _nestedDictionaryHandling;
            private readonly List<string> _excludedProperties;
            private readonly List<string> _headings = new List<string>();
            private readonly List<List<string>> _rows = new List<List<string>>();
            private readonly IDictionary<JTokenType, Func<string, string>> _typeFormatters;
            private readonly IDictionary<string, Func<string, string>> _columnFormatters;

            public Converter(
                JArray array,
                NestedArrayHandling nestedArrayHandling,
                NestedDictionaryHandling nestedDictionaryHandling,
                List<string> excludedProperties,
                IDictionary<JTokenType, Func<string, string>> typeFormatters,
                IDictionary<string, Func<string, string>> columnFormatters)
            {
                _array = array;
                _nestedArrayHandling = nestedArrayHandling;
                _nestedDictionaryHandling = nestedDictionaryHandling;
                _excludedProperties = excludedProperties;
                _typeFormatters = typeFormatters;
                _columnFormatters = columnFormatters;
            }

            public string Convert()
            {
                foreach (var obj in _array)
                {
                    var row = new List<string>();

                    foreach (var token in obj)
                    {
                        if (token.Type == JTokenType.Property)
                        {
                            PropertyHandling((JProperty)token, row);
                        }
                        else
                        {
                            continue;
                        }
                    }

                    _rows.Add(row);

                }

                var csv = RowsToCsv();

                return csv;
            }

            private void PropertyHandling(JProperty prop, List<string> row)
            {
                var key = prop.Name;
                var value = prop.Value;

                if (IsExcluded(key))
                    return;

                if (value.Type == JTokenType.Array)
                {
                    ArrayHandling(key, (JArray)value, row);
                }
                else if (value.Type == JTokenType.Object)
                {
                    DictionaryHandling(key, (JObject)value, row);
                }
                else
                {
                    var formattedValue = ApplyFormatting(key, (JValue)value);
                    Add(key, formattedValue, row);
                }
            }

            private void ArrayHandling(string key, JArray array, List<string> row)
            {
                if (_nestedArrayHandling == NestedArrayHandling.Ignore)
                {
                    AddIgnore(key, row);
                }
                else if (_nestedArrayHandling == NestedArrayHandling.ToString)
                {
                    Add(key, array.ToString(), row);
                }
                else
                {
                    throw new NotImplementedException(string.Format("Handling for NestedArrayHandling.{0} is not implemented", _nestedArrayHandling));
                }
            }

            private void DictionaryHandling(string key, JObject dict, List<string> row)
            {
                if (_nestedDictionaryHandling == NestedDictionaryHandling.Ignore)
                {
                    AddIgnore(key, row);
                }
                else if (_nestedDictionaryHandling == NestedDictionaryHandling.ToString)
                {
                    Add(key, dict.ToString(), row);
                }
                else
                {
                    throw new NotImplementedException(string.Format("Handling for NestedDictionaryHandling.{0} is not implemented", _nestedArrayHandling));
                }
            }

            private string ApplyFormatting(string key, JValue value)
            {
                Func<string, string> formatter;

                if (!_columnFormatters.TryGetValue(key, out formatter))
                {
                    _typeFormatters.TryGetValue(value.Type, out formatter);
                }

                if (formatter != null)
                {
                    return formatter(value.ToString());
                }
                else
                {
                    return value.ToString();
                }

            }

            private void Add(string key, string value, List<string> row)
            {
                var idx = _headings.IndexOf(key);

                if (idx < 0)
                {
                    idx = _headings.Count;
                    _headings.Add(key);
                }

                while (row.Count < idx)
                    row.Add(CsvFormat(string.Empty));

                row.Insert(idx, CsvFormat(value));

            }

            private void AddIgnore(string key, List<string> row)
            {
                Add(key, string.Empty, row);
            }

            private string CsvFormat(string value)
            {
                //Any single double quote (") must be replaced with double double quote ("")
                value = value.Replace("\"", "\"\"");

                //The value then needs to be enclosed in quotes
                return "\"" + value + "\"";
            }

            private bool IsExcluded(string key)
            {
                return _excludedProperties.Contains(key);
            }

            private string RowsToCsv()
            {
                if (_rows.Count == 0)
                    return string.Empty;

                var maxLen = _rows.OrderByDescending(r => r.Count).First().Count;

                var sb = new StringBuilder();

                var header = string.Join(",", _headings);
                sb.AppendLine(header);

                foreach (var row in _rows)
                {
                    while (row.Count < maxLen)
                        row.Add(CsvFormat(""));

                    var line = string.Join(",", row);
                    sb.AppendLine(line);
                }

                return sb.ToString();

            }

        }

        public class Settings
        {
            /// <summary>
            /// NestedArrayHandling and NestedDictionaryHandling will both be defaulted to Ignore
            /// </summary>
            /// <returns></returns>
            public static Settings IgnoreNested()
            {
                return new Settings(NestedArrayHandling.Ignore, NestedDictionaryHandling.Ignore);
            }

            /// <summary>
            /// NestedArrayHandling and NestedDictionaryHandling will both be defaulted to ToString
            /// </summary>
            /// <returns></returns>
            public static Settings IncludeNested()
            {
                return new Settings(NestedArrayHandling.ToString, NestedDictionaryHandling.ToString);
            }

            private readonly List<string> _excluded = new List<string>();
            private readonly TypeFormatters _typeFormatters = new TypeFormatters();
            private readonly ColumnFormatters _columnFormatters = new ColumnFormatters();

            public NestedArrayHandling NestedArrayHandling { get; set; }
            public NestedDictionaryHandling NestedDictionaryHandling { get; set; }
            public IEnumerable<string> Excluded { get { return _excluded; } }

            /// <summary>
            /// Type Formatters.
            /// Formatting applied to any data matching the type
            /// </summary>
            public TypeFormatters TypeFormatters { get { return _typeFormatters; } }

            /// <summary>
            /// Column Formatters.
            /// Formatting applied to any matching columns.
            /// Column Formatters have priority over Type Formatters
            /// </summary>
            public ColumnFormatters ColumnFormatters { get { return _columnFormatters; } }

            private Settings(NestedArrayHandling nestedArrayHandling, NestedDictionaryHandling nestedDictionaryHandling)
            {
                //Defaults
                NestedArrayHandling = nestedArrayHandling;
                NestedDictionaryHandling = nestedDictionaryHandling;
            }

            public void ExcludeProperty(string property)
            {
                if (string.IsNullOrWhiteSpace(property))
                    throw new ArgumentException("property");

                _excluded.Add(property);
            }

            public void ExcludeProperties(params string[] properties)
            {
                if (properties == null || properties.Length == 0)
                    throw new ArgumentException("properties");

                _excluded.AddRange(properties);
            }

            public void ExcludeProperties(IEnumerable<string> properties)
            {
                // ReSharper disable PossibleMultipleEnumeration
                if (properties == null || !properties.Any())
                    throw new ArgumentException("properties");

                _excluded.AddRange(properties);

                // ReSharper restore PossibleMultipleEnumeration

            }

        }

        public class ColumnFormatters
        {
            private readonly Dictionary<string, Func<string, string>> _formatters = new Dictionary<string, Func<string, string>>
                (StringComparer.OrdinalIgnoreCase);

            public IDictionary<string, Func<string, string>> Formatters { get { return _formatters; } }

            public void Add(Func<string, string> formatter, params string[] columns)
            {
                foreach (var column in columns)
                {
                    _formatters.Add(column, formatter);
                }
            }

        }

        public class TypeFormatters
        {
            public Func<DateTime, string> DateFormatter { get; set; }

            public IDictionary<JTokenType, Func<string, string>> ToDictionary()
            {
                var dic = new Dictionary<JTokenType, Func<string, string>>();

                if (DateFormatter != null)
                {
                    var actualFormatter = new Func<string, string>(s => DateFormatter(System.Convert.ToDateTime(s)));
                    dic.Add(JTokenType.Date, actualFormatter);
                }

                return dic;

            }
        }

        public enum NestedArrayHandling
        {
            Ignore,
            ToString
        }

        public enum NestedDictionaryHandling
        {
            Ignore,
            ToString
        }

    }
}
